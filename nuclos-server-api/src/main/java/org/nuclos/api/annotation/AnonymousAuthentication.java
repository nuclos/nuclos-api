//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker for a custom rest rule method. (Since Nuclos 4.41.0)
 *
 * The execution of such a method is therefore possible without a login.
 * An anonymous session is opened in Nuclos. If no "anonymous" user has been created,
 * a virtual one is used.
 * Warning, possible database queries on the user or in join with users may not work as intended.
 * In such a case, adjust the rule or specify as a requirement that an "anonymous" user must be created.
 *
 * It is NOT necessary to set the system parameter "ANONYMOUS_USER_ACCESS_ENABLED".
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AnonymousAuthentication {
	String locale() default "en_GB";
	String dataLanguage() default "en_GB";
}
