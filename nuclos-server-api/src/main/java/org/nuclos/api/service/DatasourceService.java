//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import java.util.List;
import java.util.Map;

import org.nuclos.api.datasource.Datasource;
import org.nuclos.api.datasource.DatasourceColumn;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.api.exception.BusinessException;

public interface DatasourceService {
	
	public DatasourceResult run(Class<? extends Datasource> datasourceClass) throws BusinessException;
	public DatasourceResult run(Class<? extends Datasource> datasourceClass, Map<String, Object> params) throws BusinessException;

    DatasourceResult createResult(List<Object[]> rows, List<DatasourceColumn> columns);

	DatasourceColumn createResultColumn(String name, Class clz);
}
