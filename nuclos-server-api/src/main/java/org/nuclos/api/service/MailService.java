package org.nuclos.api.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;

public interface MailService {

		void send(NuclosMail mail) throws BusinessException;
		default void send(UID outgoingEmailServer, NuclosMail mail) throws BusinessException {
			send(mail);
		}
		void send(InputStream is) throws BusinessException;
		default void send(UID outgoingEmailServer, InputStream is) throws BusinessException {
			send(is);
		}
		void transformToEml(NuclosMail mail, OutputStream outputStream) throws BusinessException;
		List<NuclosMail> receive(boolean bDeleteMails) throws BusinessException;
		default List<NuclosMail> receive(UID incomingEmailServer, boolean bDeleteMails) throws BusinessException {
			return receive(bDeleteMails);
		}
		List<NuclosMail> receive(String folderFrom, boolean bDeleteMails) throws BusinessException;
		default List<NuclosMail> receive(UID incomingEmailServer, String folderFrom, boolean bDeleteMails) throws BusinessException {
			return receive(folderFrom, bDeleteMails);
		}

}
