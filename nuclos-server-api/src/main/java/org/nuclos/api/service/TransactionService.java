package org.nuclos.api.service;

public interface TransactionService {

    void disableRuleExecutionsForCurrentTransactionOnly();

    void disableBusinessObjectMetaUpdatesForCurrentTransactionOnly();
}
