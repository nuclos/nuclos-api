package org.nuclos.api.service;

import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;

import java.util.Date;

/**
 * The {@link MandatorService} provides several methods to create Mandators
 *
 */
public interface MandatorService {

	/**

	 * @param name
	 * @return UID
	 * @throws BusinessException
	 */
	public org.nuclos.api.UID insert(String name) throws BusinessException;

	/**

	 * @param name, parentMandator
	 * @return UID
	 * @throws BusinessException
	 */
	public org.nuclos.api.UID insert(String name, NuclosMandator parentMandator) throws BusinessException;

}
