package org.nuclos.api.rule;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.datasource.DatasourceColumn;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.api.provider.DatasourceProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public interface DatasourceRule {
	String IDCOLUMN = "INTID";

    DatasourceResult getData(Map<String, Object> params);

	default Long count(SearchExpression se, Map<String, Object> params) {
		return Long.valueOf(getData(params).getRows().size());
	}

	default DatasourceResult getAll(SearchExpression se, Map<String, Object> params) {
		return getData(params);
	}

	default List<Long> getAllIds(SearchExpression se, Map<String, Object> params) {
		final DatasourceResult data = getData(params);
		int i = 0;
		for (;i < data.getColumns().size(); i++) {
			DatasourceColumn column = data.getColumns().get(i);
			if (IDCOLUMN.equals(column.getName())) {
				break;
			}
		}

		final int idxCol = i;

		return data.getRows().stream()
				.map(row -> (Long) row[idxCol])
				.collect(Collectors.toList());
	}

	default DatasourceResult getById(Map<String, Object> params) {
		final DatasourceResult data = getData(params);
		int i = 0;
		for (;i < data.getColumns().size(); i++) {
			DatasourceColumn column = data.getColumns().get(i);
			if (IDCOLUMN.equals(column.getName().toUpperCase())) {
				break;
			}
		}

		final int idxCol = i;

		return data.getRows().stream()
				.filter(row -> row[idxCol] != null && row[idxCol].equals(params.get(IDCOLUMN)))
				.map(row -> {
					List<Object[]> rows = new ArrayList<>();
					rows.add(row);
					return DatasourceProvider.createResult(rows, data.getColumns());
				}).findFirst().orElse(DatasourceProvider.createResult(new ArrayList<>(), data.getColumns()));
	}

	default DatasourceResult getByIds(Map<String, Object> params) {
		final DatasourceResult data = getData(params);
		int i = 0;
		for (;i < data.getColumns().size(); i++) {
			DatasourceColumn column = data.getColumns().get(i);
			if (IDCOLUMN.equals(column.getName().toUpperCase())) {
				break;
			}
		}

		final int idxCol = i;

		return DatasourceProvider.createResult(
				data.getRows().stream()
						.filter(row -> ((List<Long>) params.get(IDCOLUMN)).contains(row[idxCol]))
						.collect(Collectors.toList()),
				data.getColumns());
	}
}
