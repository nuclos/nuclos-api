package org.nuclos.api.rule;

import org.nuclos.api.authentication.AuthenticationResult;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.context.AuthenticationContext;
import org.nuclos.api.context.RefreshAuthenticationContext;
import org.nuclos.api.exception.BusinessException;

/**
 * An {@link AuthenticationRule} can authorize any request against our RESTful service.
 * <p>
 * The rule is called if a client does not transmit a JSESSIONID (cookie).
 * <p>
 * If there is more than one rule of this type, they are called in order of {@code javax.annotation.Priority}.
 * A thrown exception cancels the request directly. If the result of the rule is {@code null} or the username and userId are {@code null},
 * the next one is called.
 * <p>
 * By default, the session is dropped after the request. If a client supports login (store and transmit the JSESSIONID cookie),
 * you should enable the Nuclos login for the user by adding {@code .withLoginRequired(true)} to the result.
 * A login always requires a logout from the client after the work is done.
 * Are many requests expected and the authentication is a complex process (SSO token check, for example),
 * a login is recommended, or at least caching some relevant information within the rule.
 *
 * @see AuthenticationContext
 * @see AuthenticationResult
 */
public interface AuthenticationRule {

	/**
	 * @param context {@link AuthenticationContext} is the context providing all authorization-relevant attributes,
	 * such as all header information and the called URL.
	 *
	 * @return A successful authentication must identify a user. A minimum return should look like this:
	 * <pre>{@code return AuthenticationResult
	 * 	.builder()
	 * 	.withUsername("nuclos")
	 * 	.build();}</pre>
	 * <p>
	 *
	 */
	AuthenticationResult authenticate(AuthenticationContext context) throws BusinessException;

	/**
	 * A session with login whose lifetime has expired will call this method next time a request starts.
	 * <p>
	 * The lifetime can be set in the result. The default is {@code null}, which means that the system is handling the session only
	 * and a refresh is never called.
	 *
	 * @param context {@link RefreshAuthenticationContext} is the context providing all refresh-relevant attributes,
	 *                                                    similar to the {@code AuthenticationContext} but with the
	 *                                                    {@link AuthenticationResult} for the current session.
	 *                                                    You can save important values from the authentication as
	 *                                                    attribute in the result {@code .withAttribute("myAttr", "myValue")}
	 * @return {@code true} when refresh was successful, {@code false} otherwise.
	 * A {@code false} automatically results in a logout.
	 */
	default boolean refreshAuthentication(RefreshAuthenticationContext context) throws BusinessException {
		return false;
	}

}
