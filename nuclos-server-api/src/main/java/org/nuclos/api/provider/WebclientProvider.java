package org.nuclos.api.provider;

import java.net.URL;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.WebclientService;
import org.nuclos.api.webclient.WebclientUrl;

/**
 * {@link WebclientProvider} provides support for handling the Nuclos webclient
 */
public class WebclientProvider {

	private static WebclientService SERVICE;

	WebclientProvider() {
	}

	public void setWebclientService(WebclientService service) {
		this.SERVICE = service;
	}

	private static WebclientService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}

	/**
	 * @param bo
	 * @return a URL for the browser.
	 * Navigates the Nuclos webclient directly to the desired location.
	 * @throws BusinessException if the Webclient URL could not be determined.
	 * In this case, set an absolut value in the system parameter WEBCLIENT_BASEURL.
	 */
	public static <T extends BusinessObject> URL buildBusinessObjectUrl(T bo) throws BusinessException {
		return buildUrl(WebclientUrl.builder()
				.withBusinessObject(bo)
				.build());
	}

	/**
	 * @param boClass
	 * @return a URL for the browser.
	 * Navigates the Nuclos webclient directly to the desired location.
	 * @throws BusinessException if the Webclient URL could not be determined.
	 * In this case, set an absolut value in the system parameter WEBCLIENT_BASEURL.
	 */
	public static <PK, T extends BusinessObject<PK>> URL buildBusinessObjectUrl(Class<T> boClass) throws BusinessException {
		return buildUrl(WebclientUrl.builder()
				.withBusinessObject(boClass)
				.build());
	}

	/**
	 * @param boClass
	 * @param boId
	 * @return a URL for the browser.
	 * Navigates the Nuclos webclient directly to the desired location.
	 * @throws BusinessException if the Webclient URL could not be determined.
	 * In this case, set an absolut value in the system parameter WEBCLIENT_BASEURL.
	 */
	public static <PK, T extends BusinessObject<PK>> URL buildBusinessObjectUrl(Class<T> boClass, PK boId) throws BusinessException {
		return buildUrl(WebclientUrl.builder()
				.withBusinessObject(boClass, boId)
				.build());
	}

	/**
	 * @param webclientUrl
	 * @return a URL for the browser.
	 * Navigates the Nuclos webclient directly to the desired location.
	 * @throws BusinessException if the Webclient URL could not be determined.
	 * In this case, set an absolut value in the system parameter WEBCLIENT_BASEURL.
	 */
	public static URL buildUrl(WebclientUrl webclientUrl) throws BusinessException {
		return getService().buildUrl(webclientUrl);
	}

}
