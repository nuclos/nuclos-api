//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.io.File;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.service.FileService;

/**
 * {@link FileProvider} consists various methods for file-operation
 * <p>
 * This class is usually used in Rule-classes and allows API access only
 * </p>
 * @author Matthias Reichart
 */
public class FileProvider {

	private static FileService service;
	
	public void setFileService(FileService repService) {
		this.service = repService;
	}
	
	private static FileService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * This method sends a {@link NuclosFile} to the default printer
	 * 
	 * @param file {@link NuclosFile} - file to print
	 * 
	 */
	public static void print(NuclosFile file) 
			throws BusinessException {
		getService().print(file);
	}
	

	/**
	 * This method sends a {@link NuclosFile} to the given printer
	 * 
	 * @param file {@link NuclosFile} - file to print
	 * @param printername
	 */
	public static void print(NuclosFile file, String printername) 
			throws BusinessException {
		getService().print(file, printername);
	}
	
	/**
	 * This method prints a {@link NuclosFile} with the given {@link PrintProperties}
	 * 
	 * @param file {@link NuclosFile} - file to print
	 * @param printProperties {@link PrintProperties} - print properties to select the printer, copies etc.
	 */
	public static void print(NuclosFile file, PrintProperties printProperties) 
			throws BusinessException {
		getService().print(file, printProperties);
	}
	
	/**
	 * This method saves a {@link NuclosFile} in the given directory
	 * 
	 * @param file {@link NuclosFile} - file to save
	 * @param directory - directory to save file in
	 * 
	 */
	public static void save(NuclosFile file, String directory) 
			throws BusinessException {
		getService().save(file, directory);
	}

	/**
	 * Returns a new {@link NuclosFile} with the given filename and content.
	 * @param fileName
	 * @param content
	 * @return
	 * @throws BusinessException
	 */
	public static NuclosFile newFile(String fileName, byte[] content) throws BusinessException {
		return getService().newFile(fileName, content);
	}

	/**
	 * Opens and reads the data from the given file and returns a new {@link NuclosFile} with the same filename and content.
	 * Argument file must be of type file
	 * @param file
	 * @return
	 * @throws BusinessException
	 */
	public static NuclosFile newFile(File file) throws BusinessException {
		return getService().newFile(file);
	}
	
	/**
	 * Opens and reads the data from the file the given filename points to and returns a new {@link NuclosFile} with the same filename and content.
	 * Argument completeFilename must include path and filename
	 * @param completeFilename
	 * @return
	 * @throws BusinessException
	 */
	public static NuclosFile newFile(String completeFilename) throws BusinessException {
		return getService().newFile(completeFilename);
	}
	
	/**
	 * Transforms the given file to PDF.
	 * Supports DOCX and ODT.
	 * PDFs are returned without manipulation.
	 * 
	 * Only checks the file name to find out what to do.
	 * 
	 * @param file
	 * @return
	 * @throws BusinessException
	 */
	public static NuclosFile toPdf(NuclosFile file) throws BusinessException {
		return getService().toPdf(file);
	}
	
}
