package org.nuclos.api.provider;

import java.util.Date;
import java.util.Optional;

import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.UserService;

/**
 * The {@link UserProvider} provides several methods to create users and manipulate user role data
 * @author reichama
 *
 */
public class UserProvider {

	private static UserService service;
	
	public void setUserService(UserService repService) {
		this.service = repService;
	}
	
	private static UserService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}

	/**
	 * This method creates a Nuclos User and returns the id of the new object.<br>
	 * A password will be generated and a default notification Email with the password will be send.<br>
	 * A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(org.nuclos.api.common.NuclosUser user) throws BusinessException {
		return getService().insert(user);
	}

	/**
	 * This method creates a Nuclos User and returns the id of the new object.<br>
	 * Sends a default notification Email with the password.<br>
	 * A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * @param user
	 * @param password If no password is given, one will be generated. The same applies to the username.
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(org.nuclos.api.common.NuclosUser user, String password) throws BusinessException {
		return getService().insert(user, password);
	}

	/**
	 * This method creates a Nuclos User and returns the id of the new object.<br>
	 * A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * @param user
	 * @param password If no password is given, one will be generated. The same applies to the username.
	 * @param sendNotification Send a default notification Email with the password
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(org.nuclos.api.common.NuclosUser user, String password, boolean sendNotification) throws BusinessException {
		return getService().insert(user, password, sendNotification);
	}

	/**
	 * <b>Deprecated</b>: Use the insert methods with <b>NuclosUser</b>
	 * This method creates a Nuclos User and returns the id of the new object. Password is created by Nuclos automatically.
	 * Afterwards a notification Email containing the password is sent to the user.<br> 
	 * <b>Note</b>: A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * 
	 * @param username, firstname, lastname, email, passwordChangeRequired
	 * @return UID
	 * @throws BusinessException
	 */
	@Deprecated
	public static org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, Boolean passwordChangeRequired) throws BusinessException {
		return getService().insert(username, firstname, lastname, email, passwordChangeRequired);
	}
	
	/**
	 * <b>Deprecated</b>: Use the insert methods with <b>NuclosUser</b>
	 *
	 * This method creates a Nuclos User and returns the id of the new object. A password must be provided.
	 * Afterwards a notification Email containing the password is sent to the user.<br> 
	 * <b>Note</b>: A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * 
	 * @param username, firstname, lastname, email, password, passwordChangeRequired
	 * @return UID
	 * @throws BusinessException
	 */
	@Deprecated
	public static org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, String password, Boolean passwordChangeRequired) throws BusinessException {
		return getService().insert(username, firstname, lastname, email, password, passwordChangeRequired);
	}

	/**
	 * This method updates the given Nuclos User.<br>
	 * @param user
	 * @throws BusinessException
	 */
	public static void update(NuclosUser user) throws BusinessException {
		getService().update(user);
	}

	/**
	 * This method resets the password for the given Nuclos User.<br>
	 * @param user
	 * @param password If no password is given, one will be generated. The same applies to the username.
	 * @param sendNotification Send a default notification Email with the password
	 * @throws BusinessException
	 */
	public static void resetPassword(NuclosUser user, String password, boolean sendNotification) throws BusinessException {
		getService().resetPassword(user, password, sendNotification);
	}

    public static Optional<org.nuclos.api.UID> verifyPasswordResolveUserId(String username, String password) {
        return getService().verifyPasswordResolveUserId(username, password);
    }

	/**
	 * This method assigns a role to a given user. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider} 
	 * 
	 * @param role
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID grantRole(Class<? extends NuclosRole> role, NuclosUser user) throws BusinessException {
		return getService().grantRole(role,user);
	}

	/**
	 * This method assigns a role to a given user.
	 * The {@link NuclosUser} and {@link NuclosRole} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}
	 * Use the classes org.nuclos.system.User and org.nuclos.system.Role
	 *
	 * @param role
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID grantRole(NuclosRole role, NuclosUser user) throws BusinessException {
		return getService().grantRole(role,user);
	}

	/**
	 * This method assigns a mandator to a given user. The class of the user mandator is generated automatically and
	 * corresponds to an existing mandator in Nuclos. The {@link NuclosMandator} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}
	 *
	 * @param mandator
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID grantMandator(NuclosMandator mandator, NuclosUser user) throws BusinessException {
		return getService().grantMandator(mandator,user);
	}

	/**
	 * This method revokes a mandator from a given user. The class of the user mandator is generated automatically and
	 * corresponds to an existing mandator in Nuclos. The {@link NuclosMandator} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}
	 *
	 * @param mandator
	 * @param user
	 * @throws BusinessException
	 */
	public static void revokeMandator(NuclosMandator mandator, NuclosUser user) throws BusinessException {
		getService().revokeMandator(mandator,user);
	}


	/**
	 * This method checks if a mandator is granted to a given user. The class of the user mandator is generated automatically and
	 * corresponds to an existing mandator in Nuclos. The {@link NuclosMandator} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}
	 *
	 * @param mandator
	 * @param user
	 * @return true if the Mandator is granted to the given user
	 * @throws BusinessException
	 */
	public static boolean isMandatorGranted(NuclosMandator mandator, NuclosUser user) throws BusinessException {
		return getService().isMandatorGranted(mandator,user);
	}

	/**
	 * This method dispossesses a user of the given role. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}. If the user is not assigned to the role a {@link BusinessException} is thrown.
	 * 
	 * @param role
	 * @param user
	 * @throws BusinessException
	 */
	public static void revokeRole(Class<? extends NuclosRole> role, NuclosUser user) throws BusinessException {
		getService().revokeRole(role,user);
	}

	/**
	 * This method dispossesses a user of the given role. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} and {@link NuclosRole} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}. Use the classes org.nuclos.system.User and org.nuclos.system.Role.
	 * If the user is not assigned to the role a {@link BusinessException} is thrown.
	 *
	 *
	 * @param role
	 * @param user
	 * @throws BusinessException
	 */
	public static void revokeRole(NuclosRole role, NuclosUser user) throws BusinessException {
		getService().revokeRole(role,user);
	}
	
	/**
	 * This method allows to define a date on which to expire a specific user account. 
	 * The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}.
	 * 
	 * @param user
	 * @param date
	 * @throws BusinessException
	 */
	public static void expire(NuclosUser user, Date date) throws BusinessException {
		getService().expire(user,date);
	}

	/**
	 * This method allows to delete a specific user account.
	 * The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}.
	 *
	 * @param user
	 * @throws BusinessException
	 */
	public static void delete(NuclosUser user) throws BusinessException {
		getService().delete(user);
	}
	
}
