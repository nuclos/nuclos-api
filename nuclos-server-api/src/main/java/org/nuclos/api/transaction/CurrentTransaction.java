package org.nuclos.api.transaction;

import org.nuclos.api.service.TransactionService;

/**
 * {@link CurrentTransaction} provides methods for disabling Nuclos core features
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @author Kai Fibr
 */
public class CurrentTransaction {

    private static TransactionService SERVICE;

    public void setTransactionService(TransactionService service) {
        this.SERVICE = service;
    }

    private static TransactionService getService() {
        if (SERVICE == null) {
            throw new IllegalStateException("too early (missing in spring context?)");
        }
        return SERVICE;
    }

    /**
     * This method disables execution of any rules for the current transaction
     */
    public static void disableRuleExecution() {
        getService().disableRuleExecutionsForCurrentTransactionOnly();
    }

    /**
     * This method disables update of fields <code>CHANGEDBY</code>, <code>CHANGEDAT</code> and <code>VERSION</code> by the system
     * and enables the use of <code>setCreatedBy</code> and <code>setChangedBy</code> in rules
     */
    public static void disableBusinessObjectMetaUpdate() {
        getService().disableBusinessObjectMetaUpdatesForCurrentTransactionOnly();
    }

}
