package org.nuclos.api.authentication;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.nuclos.api.UID;
import org.nuclos.api.context.RefreshAuthenticationContext;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.rule.AuthenticationRule;

/**
 * This class is passed to Nuclos as the result of a rule-based authentication.
 *
 * @see AuthenticationRule
 */
public class AuthenticationResult {

	public static final long UNSET = -1l;

	private static final AtomicLong IDFACTORY = new AtomicLong(1l);

	private final long id;

	private String username;

	private UID userId;

	private UID mandatorId;

	private NuclosLocale locale = NuclosLocale.DE_DE;

	private NuclosLocale dataLanguage;

	private URI serverUri;

	private boolean loginRequired = false;

	private long lifetime = UNSET;

	private TimeUnit lifetimeUnit;

	private Map<String, String> attributesMap;

	protected AuthenticationResult() {
		this.id = IDFACTORY.incrementAndGet();
	}

	/**
	 * Only a internal id for equals and hashCode.
	 *
	 * @return the internal id of this result.
	 */
	public long getId() {
		return id;
	}

	/**
	 * Either username or user id must be specified.
	 *
	 * @return the username determined during authentication.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Either username or user id must be specified.
	 *
	 * @return the user id determined during authentication.
	 */
	public UID getUserId() {
		return userId;
	}

	/**
	 *
	 * @return the mandator id determined during authentication.
	 */
	public UID getMandatorId() {
		return mandatorId;
	}

	/**
	 *
	 * @return the locale determined during authentication.
	 */
	public NuclosLocale getLocale() {
		return locale;
	}

	/**
	 *
	 * @return the data language determined during authentication.
	 */
	public NuclosLocale getDataLanguage() {
		return dataLanguage;
	}

	/**
	 * Clients can reach the application server via this base URI.
	 * If not specified ({@code null}), the default settings are used.
	 * Maybe a reverse proxy exists between server and client and requires an adjustment.
	 * This is used to create referencing links between data.
	 *
	 * @return the server uri determined during authentication.
	 */
	public URI getServerUri() {
		return serverUri;
	}

	/**
	 *
	 * @return {@code true} if Nuclos should login the user.
	 * 			{@code false} drops the session after response.
	 * 	(Determined during authentication)
	 */
	public boolean isLoginRequired() {
		return loginRequired;
	}

	/**
	 * The default lifetime is {@code AuthenticationResult.UNSET}, which means that the system is handling
	 * the session lifetime only and a refresh is never called.
	 * @see AuthenticationRule#refreshAuthentication(RefreshAuthenticationContext)
	 *
	 * @return the lifetime
	 */
	public long getLifetime() {
		return lifetime;
	}

	/**
	 * @return the unit in which lifetime is expressed in
	 */
	public TimeUnit getLifetimeUnit() {
		return lifetimeUnit;
	}

	/**
	 * The default lifetime is {@code AuthenticationResult.UNSET}, which means that the system is handling
	 * the session lifetime only and a refresh is never called.
	 * @see AuthenticationRule#refreshAuthentication(RefreshAuthenticationContext)
	 *
	 * @param lifetime
	 * @param unit the unit that lifetime is expressed in
	 */
	public void setLifetime(long lifetime, TimeUnit unit) {
		if (lifetime < 0l && lifetime != UNSET) {
			throw new IllegalArgumentException("lifetime cannot be negative");
		}
		if (unit == null && lifetime != UNSET) {
			throw new IllegalArgumentException("lifetime without unit is not possible");
		}
		this.lifetime = lifetime;
		this.lifetimeUnit = unit;
	}

	/**
	 * Store important information during authentication, and access them later during refresh.
	 * @param attribute
	 * @param value
	 */
	public void setAttribute(String attribute, String value) {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute must not be null");
		}

		synchronized (this) {
			if (value == null) {
				if (attributesMap != null) {
					attributesMap.remove(attribute);
					if (attributesMap.isEmpty()) {
						attributesMap = null;
					}
				}
			} else {
				if (attributesMap == null) {
					attributesMap = new HashMap<>();
				}
				attributesMap.put(attribute, value);
			}
		}
	}

	/**
	 *
	 * @param attribute
	 * @return value (for the attribute)
	 */
	public String getAttribute(String attribute) {
		synchronized (this) {
			if (attributesMap == null) {
				return null;
			}
			return attributesMap.get(attribute);
		}
	}

	private void setUsername(final String username) {
		this.username = username;
	}

	private void setUserId(final UID userId) {
		this.userId = userId;
	}

	private void setMandatorId(final UID mandatorId) {
		this.mandatorId = mandatorId;
	}

	private void setLocale(final NuclosLocale locale) {
		if (locale == null) {
			throw new IllegalArgumentException("locale must not be null");
		}
		this.locale = locale;
	}

	private void setDataLanguage(final NuclosLocale dataLanguage) {
		this.dataLanguage = dataLanguage;
	}

	private void setServerUri(final URI serverUri) {
		this.serverUri = serverUri;
	}

	private void setLoginRequired(final boolean loginRequired) {
		this.loginRequired = loginRequired;
	}

	/**
	 * Constructs a new builder instance for an authentication result with default settings.
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * A builder for {@link AuthenticationResult}
	 */
	public static class Builder {

		private final AuthenticationResult result = new AuthenticationResult();

		protected Builder() {
		}

		/**
		 * Either username or user id must be specified.
		 *
		 * @param username (could be null)
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withUsername(String username) {
			result.setUsername(username);
			return this;
		}

		/**
		 * Either username or user id must be specified.
		 *
		 * @param userId (could be null)
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withUserId(UID userId) {
			result.setUserId(userId);
			return this;
		}

		/**
		 * If mandators are in use, the new session will need a selected one,
		 * similar to the login with a web- or rich-client.
		 * Otherwise, permission checks could throw errors.
		 *
		 * @param mandatorId (could be null)
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withMandatorId(UID mandatorId) {
			result.setMandatorId(mandatorId);
			return this;
		}

		/**
		 * Default is {@code NuclosLocale.DE_DE}
		 *
		 * @param locale for the new session (not null)
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withLocale(NuclosLocale locale) {
			result.setLocale(locale);
			return this;
		}

		/**
		 * Data language for the new session, if in use be the system.
		 * @param dataLanguage (could be null)
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withDataLanguage(NuclosLocale dataLanguage) {
			result.setDataLanguage(dataLanguage);
			return this;
		}

		/**
		 * Clients can reach the application server via this base URI.
		 * If not specified ({@code null}), the default settings are used.
		 * Maybe a reverse proxy exists between server and client and requires an adjustment.
		 * This is used to create referencing links between data.
		 *
		 * @param serverUri (could be null)
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withServerUri(URI serverUri) {
			result.setServerUri(serverUri);
			return this;
		}

		/**
		 * @see AuthenticationRule
		 *
		 * @param loginRequired
		 *              {@code true} if Nuclos should login the user.
		 * 				{@code false} (Default) drops the session after response.
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withLoginRequired(boolean loginRequired) {
			result.setLoginRequired(loginRequired);
			return this;
		}

		/**
		 * The default lifetime is {@code AuthenticationResult.UNSET}, which means that the system is handling
		 * the session lifetime only and a refresh is never called.
		 * @see AuthenticationRule#refreshAuthentication(RefreshAuthenticationContext)
		 *
		 * @param lifetime
		 * @param unit the unit that lifetime is expressed in
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withLifetime(long lifetime, TimeUnit unit) {
			result.setLifetime(lifetime, unit);
			return this;
		}

		/**
		 * You can store important information here and access it later during a refresh.
		 * @param attribute
		 * @param value
		 * @return this {@code Builder} instance (for chaining)
		 */
		public Builder withAttribute(String attribute, String value) {
			result.setAttribute(attribute, value);
			return this;
		}

		/**
		 *
		 * @return the {@code AuthenticationResult} with the requested parameters.
		 */
		public AuthenticationResult build() {
			return result;
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final AuthenticationResult that = (AuthenticationResult) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("AuthenticationResult{");
		sb.append("id=").append(id);
		sb.append(", username='").append(username).append('\'');
		sb.append(", userId=").append(userId);
		sb.append(", mandatorId=").append(mandatorId);
		sb.append(", locale=").append(locale);
		sb.append(", dataLanguage=").append(dataLanguage);
		sb.append(", serverUri=").append(serverUri);
		sb.append(", loginRequired=").append(loginRequired);
		sb.append(", lifetime=").append(lifetime);
		sb.append(", lifetimeUnit=").append(lifetimeUnit);
		sb.append(", attributesMap=").append(attributesMap);
		sb.append('}');
		return sb.toString();
	}

}
