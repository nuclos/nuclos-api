package org.nuclos.api.context;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.nuclos.api.rule.AuthenticationRule;

/**
 * {@link AuthenticationContext} represents the context used in an
 * {@link AuthenticationRule#authenticate(AuthenticationContext)}.
 * <p>
 * It contains information that can be used to authenticate a request.
 */
public interface AuthenticationContext extends LogContext {

	/**
	 * Returns an injectable interface that provides access to HTTP header information.
	 * @see HttpHeaders
	 *
	 * @return all http headers from the request
	 */
	HttpHeaders getHttpHeaders();

	/**
	 * Returns an injectable interface that provides access to application and request
	 * URI information. Relative URIs are relative to the base URI of the application.
	 * @see UriInfo#getBaseUri()
	 *
	 * @return the URI from the request
	 */
	UriInfo getUriInfo();

	/**
	 * Returns a string containing the unique identifier assigned
	 * to this session. The identifier is assigned
	 * by the servlet container and is implementation dependent.
	 * @see javax.servlet.http.HttpSession#getId()
	 *
	 * @return a string specifying the identifier
	 * assigned to this session
	 */
	String getSessionId();

	/**
	 * Returns the Internet Protocol (IP) address of the client
	 * or last proxy that sent the request.
	 * For HTTP servlets, same as the value of the
	 * CGI variable {@code REMOTE_ADDR}.
	 * @see javax.servlet.ServletRequest#getRemoteAddr()
	 *
	 * @return a {@code String} containing the
	 * IP address of the client that sent the request
	 */
	String getRemoteAddr();

	/**
	 * Returns the fully qualified name of the client
	 * or the last proxy that sent the request.
	 * If the engine cannot or chooses not to resolve the hostname
	 * (to improve performance), this method returns the dotted-string form of
	 * the IP address. For HTTP servlets, same as the value of the CGI variable
	 * {@code REMOTE_HOST}.
	 * @see javax.servlet.ServletRequest#getRemoteHost()
	 *
	 * @return a {@code String} containing the fully
	 * qualified name of the client
	 */
	String getRemoteHost();

	/**
	 * Returns the login of the user making this request, if the
	 * user has been authenticated, or {@code null} if the user
	 * has not been authenticated.
	 * Whether the user name is sent with each subsequent request
	 * depends on the browser and type of authentication. Same as the
	 * value of the CGI variable REMOTE_USER.
	 * @see javax.servlet.http.HttpServletRequest#getRemoteUser()
	 *
	 * @return a {@code String} specifying the login
	 * of the user making this request, or {@code null}
	 * if the user login is not known
	 */
	String getRemoteUser();
}
