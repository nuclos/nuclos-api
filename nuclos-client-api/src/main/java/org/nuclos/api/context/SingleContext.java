package org.nuclos.api.context;

public interface SingleContext<PK> {

	PK getObjectId();
}
