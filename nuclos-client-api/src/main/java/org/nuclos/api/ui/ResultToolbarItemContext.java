package org.nuclos.api.ui;

import java.util.Collection;

public interface ResultToolbarItemContext {
	/**
	* Returns the selection of the result list
	* @return
	* Collection of org.nuclos.common.dal.vo.EntityObjectVO (Sorry, no
	API)
	*/
	Collection<Object> getSelectedResultItems();
	/**
	* Returns the search condition of the current result list
	* @return
	* SearchCondition of
	type org.nuclos.common.collect.collectable.searchcondition.Collectab
	leSearchCondition (Sorry, no API)
	*/
	Object getSearchCondition();
	/**
	* Lock the current result list (tab)
	* @param locked
	*/
	void setUiLocked(boolean locked);
	/**
	* Reloads the given result item from server.
	* (Removes the item from view, if not found any more.)
	* The selection is untouched.
	* @param resultItem
	*/
	void reloadResultItem(Object resultItem);
	/**
	* Reloads (Refresh) the complete result list from server.
	* Removes any selection!
	*/
	void reloadResultList();
}