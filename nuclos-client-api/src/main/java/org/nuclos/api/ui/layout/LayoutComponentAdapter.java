//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ui.layout;

import org.nuclos.api.context.MultiContext;
import org.nuclos.api.context.SingleContext;

/**
 * Layout Component Adapter
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 *
 */
public class LayoutComponentAdapter<PK> implements LayoutComponentListener<PK> {

	@Override
	public void singleViewEntered(SingleContext<PK> context) {
	}

	@Override
	public void multiViewEntered(MultiContext<PK> context) {
	}

	@Override
	public void searchEntered(SearchContext context) {
	}

	@Override
	public void newEntered(NewContext context) {
	}

	@Override
	public void singleDeleteBefore(SingleContext<PK> context) {
	}

	@Override
	public void singleDeleteAfter(SingleContext<PK> context) {
	}

	@Override
	public void multiDeleteBefore(MultiContext<PK> context) {
	}

	@Override
	public void multiDeleteAfter(MultiContext<PK> context) {
	}

	@Override
	public void singleUpdateBefore(SingleContext<PK> context) {
	}

	@Override
	public void singleUpdateAfter(SingleContext<PK> context) {
	}

	@Override
	public void singleInsertBefore(SingleContext<PK> context) {
	}

	@Override
	public void singleInsertAfter(SingleContext<PK> context) {
	}

}
