package org.nuclos.api.ui;

import java.util.Collection;

public interface ResultToolbarItem {

	/**
	 * Returns true if button is usable based on the currently selected rows in result list.
	 * @param currentSelection
	 * @return true if button is usable based on the currently selected rows in result list
	 */
	public boolean isEnabled(Collection<Object> currentSelection);
	
	/**
	 * Perform action based on context.
	 * @param context
	 */
	public void action(ResultToolbarItemContext context);
}
