//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import org.nuclos.api.Direction;
import org.nuclos.api.Message;
import org.nuclos.api.Progress;
import org.nuclos.api.command.Command;

/**
 * TODO: This does not only send simple messages but also other client commands and should therefore be renamed.
 */
public interface MessageContextService {

	void sendMessage(Message message);
	
	void sendProgress(Progress progress);
	
	void navigate(Direction direction);

	void sendCommand(Command command);
}
