package org.nuclos.api.service;

import java.net.URL;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.webclient.WebclientUrl;

public interface WebclientService {

	URL buildUrl(WebclientUrl webclientUrl) throws BusinessException;

}
