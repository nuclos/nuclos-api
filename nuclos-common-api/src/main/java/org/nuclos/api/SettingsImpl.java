//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class SettingsImpl extends ConcurrentHashMap<String, Serializable>
	implements Settings {
		
	private static final Logger LOG = Logger.getLogger(SettingsImpl.class);
	
	private static final long serialVersionUID = -7094457981078544249L;
	
	@Override
	public void setSetting(String setting, Serializable value) {
		if (value == null) {
			remove(setting);
		} else {
			put(setting, value);
		}
	}
	
	@Override
	public Serializable getSetting(String setting) {
		return get(setting);
	}
	
	@Override
	public <S extends Serializable> S getSetting(String setting, Class<S> cls) {
		final Object value = get(setting);
		try {
			return cls.cast(value);
		}
		catch (ClassCastException e) {
			LOG.error("On " + this + " field " + setting + " value " + value + " expected type " + cls, e);
			throw e;
		}
	}
	
	@Override
	public Set<String> getSettingNames() {
		return keySet();
	}
}
