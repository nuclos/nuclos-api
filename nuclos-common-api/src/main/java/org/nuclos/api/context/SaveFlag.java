package org.nuclos.api.context;

import java.util.Arrays;

/**
 * Created by Oliver Brausch on 27.04.24.
 */
public enum SaveFlag {
	SKIP_RULES,
	PATCH;

	public boolean isIn(SaveFlag... saveFlags) {
		return saveFlags != null && saveFlags.length > 0 && Arrays.asList(saveFlags).contains(this);
	}
}
