//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;
import org.nuclos.api.statemodel.State;

/**
 * {@link StateChangeContext} represents the context used in StateChange-Rule, StateChange and StateChangeFinal
 * <p>It contains the TargetState and the SourceState and several functions like caching
 *
 * @see RuleContext
 * @author Matthias Reichart
 */
public interface StateChangeContext extends RuleContext {
	

	/**
	 * This method returns the state uid that has to be changed
	 * @return State
	 * @deprecated use method {@link #getSourceState()}
	 */
	public UID getSourceStateId();
	
	/**
	 * This method returns the state that has to be changed
	 * @return State
	 */
	public State getSourceState();
	
	/**
	 * This method returns the state number that has to be changed
	 * @return State 
	 * @deprecated use method {@link #getSourceState()}
	 */
	@Deprecated
	public Integer getSourceStateNumber();
	
	/**
	 * This method returns the state id the user wants to change to
	 * @deprecated use method {@link #getTargetState()}
	 */
	public UID getTargetStateId();
	
	/**
	 * This method returns the state the user wants to change to
	 */
	public State getTargetState();
	
	/**
	 * This method returns the state number the user wants to change to
	 * @return
	 * @deprecated use method {@link #getTargetState()}
	 */
	@Deprecated
	public Integer getTargetStateNumber();

	/**
	 * This method returns the {@link BusinessObject} that State should be changed
	 * 
	 * @param t Classtype being a {@link Stateful}
	 * @return {@link BusinessObject} extending {@link Stateful}
	 */
	public <T extends Stateful> T getBusinessObject(Class<T> t);

	/**
	 * This method returns the {@link GenericBusinessObject} that State should be changed
	 *
	 * @param t Classtype being a {@link GenericBusinessObject}
	 * @return {@link GenericBusinessObject} extending {@link GenericBusinessObject}
	 */
	public <T extends GenericBusinessObject> T getGenericBusinessObject(Class<T> t) throws BusinessException;

	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 * @param prio
	 */
	public void notify(String message, Priority prio);
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos as a message with priority 'normal'. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 */
	public void notify(String message);
	
}
