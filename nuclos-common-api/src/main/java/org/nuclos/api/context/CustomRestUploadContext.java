/**
 * {@link CustomRestUploadContext} represents the context used in {@link org.nuclos.api.rule.CustomRestRule}
 * when sending MULTIPART_FORM_DATA
 */
package org.nuclos.api.context;

import java.io.InputStream;

public interface CustomRestUploadContext extends CustomRestContext {

	/**
	 * file name of uploaded file
	 */
	String getFilename();

	/**
	 * stream of data sent by upload
	 */
	InputStream getInputStream();
}

