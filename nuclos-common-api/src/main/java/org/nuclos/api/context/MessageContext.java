//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.net.URL;

import org.nuclos.api.Direction.AdditionalNavigation;
import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.command.CloseCommand;
import org.nuclos.api.command.Command;
import org.nuclos.api.service.MessageContextService;

/**
 * {@link MessageContext} adds support for sending commands to the client.
 */
public class MessageContext {
		
	private static MessageContextService SERVICE;
	
	MessageContext() {
	}
	
	public void setMessageContextService(MessageContextService service) {
		this.SERVICE = service;
	}
	
	public static void sendMessage(String message) {
		sendMessage(message, null);
	}
	
	public static void sendMessage(String message, String title) {
		getService().sendMessage(new MessageImpl(message, title, true));
	}
	
	public static void sendProgress(int percent, String message) {
		getService().sendProgress(new ProgressImpl(percent, message));
	}
	
	public static void navigate(BusinessObject<?> businessObject) {
		getService().navigate(new DirectionImpl<>(businessObject, false));
	}
	
	public static void navigate(BusinessObject<?> businessObject, boolean newTab) {
		getService().navigate(new DirectionImpl<>(businessObject, newTab));
	}

	public static void navigate(BusinessObject<?> businessObject, UID searchFilterUid) {
		getService().navigate(new DirectionImpl<>(businessObject, searchFilterUid, false));
	}

	public static void navigate(BusinessObject<?> businessObject, UID searchFilterUid, boolean newTab) {
		getService().navigate(new DirectionImpl<>(businessObject, searchFilterUid, newTab));
	}
	
	public static void navigate(AdditionalNavigation additionalNavigation) {
		getService().navigate(new DirectionImpl<>(additionalNavigation, false));
	}

	public static void navigate(AdditionalNavigation additionalNavigation, boolean newTab) {
		getService().navigate(new DirectionImpl<>(additionalNavigation, newTab));
	}

	public static void navigate(URL url) {
		getService().navigate(new DirectionImpl<>(url, false));
	}

	public static void navigate(URL url, boolean newTab) {
		getService().navigate(new DirectionImpl<>(url, newTab));
	}

	/**
	 * Sends an {@link org.nuclos.api.command.CloseCommand} to the client.
	 */
	public static void sendClose() {
		sendCommand(new CloseCommand());
	}

	/**
	 * Sends the given command to the client.
	 *
	 * @param command
	 */
	public static void sendCommand(Command command) {
		getService().sendCommand(command);
	}

	private static MessageContextService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}
	
	private static class MessageImpl implements org.nuclos.api.Message {
		
		private static final long serialVersionUID = 11454841123565L;

		private final String message;
		
		private final String title;
		
		private final boolean overlay;

		/**
		 * for deserialization only
		 */
		protected MessageImpl() {
			this(null, null, false);
		}

		public MessageImpl(String message, String title, boolean overlay) {
			super();
			this.message = message;
			this.title = title;
			this.overlay = overlay;
		}

		public String getMessage() {
			return message;
		}
		
		@Override
		public String getTitle() {
			return title;
		}

		public boolean isOverlay() {
			return overlay;
		}

		@Override
		public String toString() {
			return "MessageImpl[message=" + getMessage() + ",title=" + getTitle() + ",overlay=" + isOverlay() + "]";
		}
		
	}
	
	private static class ProgressImpl implements org.nuclos.api.Progress {
		
		private static final long serialVersionUID = 11454841123345L;

		private final String message;
		
		private final int percent;

		/**
		 * for deserialization only
		 */
		protected ProgressImpl() {
			this(0, null);
		}

		public ProgressImpl(int percent, String message) {
			super();
			this.percent = percent;
			this.message = message;
		}

		@Override
		public String getMessage() {
			return message;
		}
		
		@Override
		public int getPercent() {
			return percent;
		}

		@Override
		public String toString() {
			return "ProgressImpl[message=" + getMessage() + ",percent=" + getPercent() + "]";
		}
		
	}
	
	private static class DirectionImpl<PK> implements org.nuclos.api.Direction<PK> {

		private static final long serialVersionUID = 77454841123577L;

		private final PK id;
		private final UID entityUid;
		private final String entity;
		private final UID searchFilterUid;
		private final boolean newTab;
		private final AdditionalNavigation additionalNavigation;
		private final URL url;

		/**
		 * for deserialization only
		 */
		protected DirectionImpl() {
			this.id = null;
			this.entityUid = null;
			this.entity = null;
			this.searchFilterUid = null;
			this.additionalNavigation = null;
			this.newTab = false;
			this.url = null;
		}
		
		public DirectionImpl(BusinessObject<PK> businessObject, boolean newTab) {
			super();
			this.id = businessObject.getId();
			this.entityUid = businessObject.getEntityUid();
			this.entity = businessObject.getEntity();
			this.searchFilterUid = null;
			this.additionalNavigation = null;
			this.newTab = newTab;
			this.url = null;
		}

		public DirectionImpl(BusinessObject<PK> businessObject, UID searchFilterUid, boolean newTab) {
			super();
			this.id = businessObject.getId();
			this.entityUid = businessObject.getEntityUid();
			this.entity = businessObject.getEntity();
			this.searchFilterUid = searchFilterUid;
			this.additionalNavigation = null;
			this.newTab = newTab;
			this.url = null;
		}
		
		public DirectionImpl(AdditionalNavigation additionalNavigation, boolean newTab) {
			super();
			this.id = null;
			this.entityUid = null;
			this.entity = null;
			this.searchFilterUid = null;
			this.additionalNavigation = additionalNavigation;
			this.newTab = newTab;
			this.url = null;
		}

		public DirectionImpl(final URL url, final boolean newTab) {
			super();
			this.id = null;
			this.entityUid = null;
			this.entity = null;
			this.searchFilterUid = null;
			this.additionalNavigation = null;
			this.newTab = newTab;
			this.url = url;
		}

		@Override
		public PK getId() {
			return id;
		}

		@Override
		public UID getEntityUid() {
			return entityUid;
		}

		@Override
		public String getEntity() {
			return entity;
		}

		@Override
		public UID getSearchFilterUid() {
			return searchFilterUid;
		}

		@Override
		public boolean isNewTab() {
			return newTab;
		}

		@Override
		public org.nuclos.api.Direction.AdditionalNavigation getAdditionalNavigation() {
			return additionalNavigation;
		}

		@Override
		public URL getUrl() {
			return url;
		}

		@Override
		public String toString() {
			return "DirectionImpl[id=" + id + ",entityUid= " + entityUid + ",entity=" + entity + ",searchFilterUid=" + searchFilterUid
					+ ",additionalNavigation=" + additionalNavigation + ",url=" + url + "]";
		}
		
	}
}
