//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PreDestroy;

/**
 * {@link SpringInputContext} adds support for requesting input from the user.
 * To check if the creator of the request that is executed is able to provide 
 * additional information, use <code>isSupported()</code>.
 * <p>
 * There is only one instance of this class (i.e. a singleton). Compared to 
 * all-static methods, this works better in a Spring-driven environment.
 * </p>
 * @see 	InputSpecification
 * @see 	InputRequiredException
 * @author	thomas.schiffmann
 */
public class SpringInputContext {

	private static SpringInputContext INSTANCE = new SpringInputContext();
	
	//
	
	private ThreadLocal<Boolean> supported = new ThreadLocal<Boolean>() {
		@Override
		protected Boolean initialValue() {
			return Boolean.FALSE;
		}
	};

	private ThreadLocal<Map<String, Serializable>> threadLocal = new ThreadLocal<Map<String, Serializable>>() {
		
		private List<WeakReference<Map<String,Serializable>>> maps = new LinkedList<WeakReference<Map<String,Serializable>>>();
		
		@Override
		protected Map<String, Serializable> initialValue() {
			final Map<String, Serializable> result = new HashMap<String, Serializable>();
			maps.add(new WeakReference<Map<String,Serializable>>(result));
			return result;
		}
		
		@Override
		public synchronized void finalize() {
			if (maps == null) return;
			
			for (WeakReference<Map<String,Serializable>> ref: maps) {
				final Map<String,Serializable> map = ref.get();
				if (map != null) {
					map.clear();
				}
			}
			maps = null;
		}
	};
	
	//
	
	private SpringInputContext() {
	}
	
	public static SpringInputContext getInstance() {
		return INSTANCE;
	}
	
	//

	public void set(Map<String, Serializable> value) {
		threadLocal.get().putAll(value);
	}

	/**
	 * Get value of requested variable
	 * @param key Requested variable
	 * @return Value of requested variable, null if not answered.
	 */
	public Serializable get(String key) {
		return threadLocal.get().get(key);
	}

	public void clear() {
		threadLocal.get().clear();
	}

	public void setSupported(boolean value) {
		supported.set(Boolean.valueOf(value));
	}

	/**
	 * Is the creator of the request able to provide additional input?
	 * @return True, if request creator can handle {@link InputRequiredException}s, otherwise false.
	 */
	public boolean isSupported() {
		return supported.get() != null ? (boolean) supported.get() : false;
	}
	
	@PreDestroy
	public synchronized void destroy() {
		if (INSTANCE == null) return;
		
		if (threadLocal != null) {
			threadLocal.remove();
		}
		if (supported != null) {
			supported.remove();
		}
		// 
		threadLocal = null;
		supported = null;
		INSTANCE = null;
	}
}
