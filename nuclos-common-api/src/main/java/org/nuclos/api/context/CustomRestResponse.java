package org.nuclos.api.context;

import java.util.HashMap;
import java.util.Map;

/**
 * response of custom REST service containing additional header information
 */
public class CustomRestResponse {

	private final Object responseValue;

	private final Map<String, String> headers = new HashMap<>();

	public CustomRestResponse(final Object responseValue) {
		this.responseValue = responseValue;
	}

	public Object getResponseValue() {
		return responseValue;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public String getHeader(final String headerName) {
		return this.headers.get(headerName);
	}

	public void setHeader(final String headerName, final String headerValue) {
		this.headers.put(headerName, headerValue);
	}
}
