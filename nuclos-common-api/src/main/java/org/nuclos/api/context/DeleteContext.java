//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;

/**
 * {@link DeleteContext} represents the context used in Deletion-Rule
 * <p>It contains the {@link BusinessObject} to delete and several functions like caching
 *
 * @see RuleContext
 * @author Matthias Reichart
 */
public interface DeleteContext extends RuleContext {
	
	/**
	 * This method returns the {@link BusinessObject} containing the entry-data that has to be deleted. 
	 *
	 * @param t Classtype being a {@link BusinessObject}
	 * @return {@link BusinessObject}
	 */
	<T extends BusinessObject> T getBusinessObject(Class<T> t);

	/**
	 * This method returns the {@link GenericBusinessObject} containing the entry-data that has to be deleted.
	 *
	 * @param t Classtype being a {@link GenericBusinessObject}
	 * @return {@link GenericBusinessObject} extending {@link GenericBusinessObject}
	 */
	public <T extends GenericBusinessObject> T getGenericBusinessObject(Class<T> t) throws BusinessException;

	/**
	 * This method returns wether the deletion is logical or physical
	 * @return true or false
	 */
	boolean isLogical();
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 * @param prio
	 */
	void notify(String message, Priority prio);
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos as a message with priority 'normal'. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 */
	void notify(String message);
}
