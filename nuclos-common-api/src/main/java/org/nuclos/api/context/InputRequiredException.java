//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

/**
 * Exception to request additional input user.
 *
 * @author	thomas.schiffmann
 */
public class InputRequiredException extends RuntimeException {

	private static final long serialVersionUID = -5734456430506412789L;

	private final InputSpecification inputSpecification;
	
	private final InputDelegateSpecification inputDelegateSpecification;

	/**
	 * for deserialization only
	 */
	protected InputRequiredException() {
		this.inputSpecification = null;
		this.inputDelegateSpecification = null;
	}

	/**
	 * Create a {@link InputRequiredException} with a {@link InputSpecification}
	 * @param inputSpecification The specification of the input dialog including the variable key.
	 * @see InputSpecification
	 */
	public InputRequiredException(InputSpecification inputSpecification) {
		assert inputSpecification != null : "inputSpecification must not be null";
		this.inputSpecification = inputSpecification;
		this.inputDelegateSpecification = null;
	}

	/**
	 * Create a {@link InputRequiredException} with a {@link InputDelegateSpecification}
	 * @param inputDelegateSpecification The specification the client needs to create the delegate instance.
	 * @see InputDelegateSpecification
	 */
	public InputRequiredException(InputDelegateSpecification inputDelegateSpecification) {
		assert inputDelegateSpecification != null : "inputDelegateSpecification must not be null";
		this.inputDelegateSpecification = inputDelegateSpecification;
		this.inputSpecification = null;
	}

	/**
	 * Get {@link InputSpecification}
	 * @return The input specification
	 */
	public InputSpecification getInputSpecification() {
		return inputSpecification;
	}
	
	/**
	 * Get {@link InputDelegateSpecification}
	 * @return The delegate specification
	 */
	public InputDelegateSpecification getInputDelegateSpecification() {
		return inputDelegateSpecification;
	}
}
