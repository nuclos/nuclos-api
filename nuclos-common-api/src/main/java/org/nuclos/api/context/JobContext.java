//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.UID;
import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.rule.TransactionalJobRule;

/**
 * {@link JobContext} represents the context used in Job-Rule
 * <p>It contains the SessionId
 *
 * @author Matthias Reichart
 */
public interface JobContext extends LogContext {

	/**
	 * This method sets the SessionId that is passed as argument
	 * @param sessionId
	 */
	public void setSessionId(Long sessionId);
	
	/**
	 * This methods return the SessionId that has been set by the user
	 * @return
	 */
	public Long getSessionId();

	/**
	 * Returns the mandator of this job
	 * @return
	 */
	public NuclosMandator getMandator();

	/**
	 * This method logs an info message into the Job-Message database 
	 * and can be viewed via JobController in Nuclos by opending the corresponding Job
	 * 
	 * @param message
	 */
	public void joblog(String message);
	
	/**
	 * This method logs a warning message into the Job-Message database 
	 * and can be viewed via JobController in Nuclos by opending the corresponding Job
	 * 
	 * @param message
	 */
	public void joblogWarn(String message);
	
	/**
	 * This method logs an error message into the Job-Message database 
	 * and can be viewed via JobController in Nuclos by opending the corresponding Job
	 * 
	 * @param message
	 */
	public void joblogError(String message);
	
	/**
	 * This methods returns the transactional object. See also {@link TransactionalJobRule}
	 * @return
	 */
	public Object getTransactionalObject();
	
	/**
	 * This methods returns <code>true</code> when the last transactional object will be returned by <code>getTransactionalObject</code>. See also {@link TransactionalJobRule}
	 * @return
	 */
	public boolean isLastTransactionalObject();
	
}
