package org.nuclos.api.businessobject.facade.thin;

import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.api.statemodel.State;

/**
 * Stateful is the interface a {@link BusinessObject} must implement to be 
 * used in {@link StateChangeRule} and {@link StateChangeFinalRule} rules
 * 
 * @since Nuclos 4.11
 * 		Use the new 'thin' property for your business object to improve the performance!
 */
public interface Stateful extends BusinessObject<Long> {

	/**
	 * Returns the id of the current status
	 * @return
	 */
	UID getNuclosStateId();
	
	/**
	 * Changes Status.
	 *
	 * @param status: The target status.
	 * @throws BusinessException
	 */
	void changeStatus(State status) throws BusinessException;

	/**
	 * Inserts the given file as generic document with the given comment
	 * @param nuclosFile file to insert
	 * @param comment
	 */
	void insertAttachment(NuclosFile nuclosFile, String comment);

	/**
	 * Returns all generic documents attached to this businessobject
	 * @return all generic documents attached to this businessobject
	 */
	List<NuclosFile> getAttachments();

	/**
	 * Deletes the given file if attached to this businessobject
	 * @param nuclosFile the file to be deleted
	 */
	void deleteAttachment(NuclosFile nuclosFile);
}
