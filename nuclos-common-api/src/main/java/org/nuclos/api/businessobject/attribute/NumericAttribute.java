//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject.attribute;

import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;

/**
 * {@link NumericAttribute} is used if the attribute type provides general numeric comparison operations
 * 
 * <p> 
 * @author Matthias Reichart
 */
public class NumericAttribute<T> extends Attribute<T> {

	

	public NumericAttribute(String pName, String pPackageName,
			String entityUid, String attributeUid, Class<T> pType) {
		super(pName, pPackageName, entityUid, attributeUid, pType);
	}

	/**
	 * LowerThan - comparison operation
	 * @param value
	 * @return
	 */
	public SearchExpression<T> Lt(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.LT);
	}
	
	/**
	 * LowerThanEqual - comparison operation
	 * @param value
	 * @return
	 */
	public SearchExpression<T> Lte(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.LTE);
	}
	
	/**
	 * GreaterThanEqual - comparison operation
	 * @param value
	 * @return
	 */
	public SearchExpression<T> Gte(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.GTE);
	}
	
	/**
	 * GreaterThan - comparison operation
	 * @param value
	 * @return
	 */
	public SearchExpression<T> Gt(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.GT);
	}
	
}
