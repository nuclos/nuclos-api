//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject.facade;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;

/**
 * Modifiable is the interface a {@link BusinessObject} must implement to be used in modifying 
 * rules, {@link UpdateRule}, {@link InsertRule}, {@link UpdateFinalRule}, {@link InsertFinalRule}
 * 
 * @author Matthias Reichart
 */
public interface Modifiable<PK> extends BusinessObject<PK> {

	/**
	 * Sets name of user who created the object
	 * @param username
	 */
	void setCreatedBy(String username);

	/**
	 * Sets name of user who last modified the object
	 * @param username
	 */
	void setChangedBy(String username);

	/**
	 * Saves this bo. A new BO will be created, an existing BO will be updated.
	 * In case of creation the BO will get the new ID.
	 *
	 * @throws BusinessException
	 */
	void save(org.nuclos.api.context.SaveFlag... saveFlags) throws BusinessException;

	/**
	 * Deletes this bo.
	 * 
	 * @throws BusinessException
	 */
	void delete() throws BusinessException;

	/**
	 * Deletes the bo with this id. Static method, no instance needed
	 *
	 * @throws BusinessException
	 */
	static <PK> void delete(PK id) throws BusinessException {
	}

	/**
	 * Sets the default values, which were specified for this modifiable in the BO editor.
	 * @return the instance of the modifiable with its defaults set
	 */
	Modifiable<PK> withDefaults();

}
