//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject;


/**
 * This class represents a dependent relation in {@link BusinessObject}
 * 
 */
public class Dependent<T> {
	
	private final String constantName;
	private final String packageName;
	private final String entityName;
	private final String entityUid;
	private final String foreignAttributeName;
	private final String foreignAttributeUid;
	private Class<T> type;

	public Dependent(String constantName, String pPackageName, String pEntityName, String pEntityUid, 
					 String pForeignAttributeName, String pForeignAttributeUid, Class<T> pType) {
		this.constantName = constantName;
		this.packageName = pPackageName;
		this.entityName = pEntityName;
		this.entityUid = pEntityUid;
		this.foreignAttributeName = pForeignAttributeName;
		this.foreignAttributeUid = pForeignAttributeUid;
		this.type = pType;
	}
	
	/**
	 * for internal use only
	 */
	public String getConstantName() {
		return constantName;
	}

	/**
	 * for internal use only
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * for internal use only
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * for internal use only
	 */
	public String getEntityUid() {
		return entityUid;
	}

	/**
	 * for internal use only
	 */
	public String getForeignAttributeName() {
		return foreignAttributeName;
	}

	/**
	 * for internal use only
	 */
	public String getForeignAttributeUid() {
		return foreignAttributeUid;
	}
	
	/**
	 * for internal use only
	 */
	public Class<T> getType() {
		return type;
	}

}
