package org.nuclos.api.businessobject.facade;

import org.nuclos.api.businessobject.BusinessObject;

/**
 * LogicalDeletable is the interface a {@link BusinessObject} must implement to be deleted logically 
 * via org.nuclos.api.provider.BusinessObjectProvider .
 * 
 * <p> 
 * @author Matthias Reichart
 */
public interface LogicalDeletable<PK>  extends BusinessObject<PK> {

	/**
	 * returns true if business object is marked as logically deleted
	 */
	Boolean getNuclosLogicalDeleted();
}
