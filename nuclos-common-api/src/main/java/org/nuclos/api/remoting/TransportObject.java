//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.remoting;

/**
 * Classes implementing this interface can be mentioned in the new remoting as JSON Type.
 * Important, your class must be in the package "org.nuclet" and the name of the lib must
 * be in the format "*-remoting-*.jar" or "*-common-*.jar", so that the new remoting class
 * search can find your transport class successfully.
 */
public interface TransportObject {

}
