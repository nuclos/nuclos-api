package org.nuclos.api.webclient;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Process;

public class WebclientUrl {

	private Object businessObjectId;

	private UID entityId;

	private UID processId;

	private String searchFilterId;

	private String taskListId;

	public UID getEntityId() {
		return entityId;
	}

	public void setEntityId(final UID entityId) {
		this.entityId = entityId;
	}

	public Object getBusinessObjectId() {
		return businessObjectId;
	}

	public void setBusinessObjectId(final Object businessObjectId) {
		this.businessObjectId = businessObjectId;
	}

	public UID getProcessId() {
		return processId;
	}

	public void setProcessId(final UID processId) {
		this.processId = processId;
	}

	public String getSearchFilterId() {
		return searchFilterId;
	}

	public void setSearchFilterId(final String searchFilterId) {
		this.searchFilterId = searchFilterId;
	}

	public String getTaskListId() {
		return taskListId;
	}

	public void setTaskListId(final String taskListId) {
		this.taskListId = taskListId;
	}

	/**
	 * Constructs a new builder instance for a Webclient URL
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * A builder for {@link WebclientUrl}
	 */
	public static class Builder {

		private final WebclientUrl result = new WebclientUrl();

		protected Builder() {
		}

		public <T extends BusinessObject> Builder withBusinessObject(T bo) {
			if (bo == null) {
				throw new IllegalArgumentException("bo must not be null");
			}
			if (bo.getId() == null) {
				throw new IllegalArgumentException("bo id must not be null");
			}
			result.setEntityId(bo.getEntityUid());
			result.setBusinessObjectId(bo.getId());
			return this;
		}

		public <PK, T extends BusinessObject<PK>> Builder withBusinessObject(Class<T> boClass) {
			if (boClass == null) {
				throw new IllegalArgumentException("bo class must not be null");
			}
			try {
				result.setEntityId(boClass.getDeclaredConstructor().newInstance().getEntityUid());
			} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
				throw new RuntimeException(e);
			}
			return this;
		}

		public <PK, T extends BusinessObject<PK>> Builder withBusinessObject(Class<T> boClass, PK boId) {
			withBusinessObject(boClass);
			if (boId == null) {
				throw new IllegalArgumentException("bo id must not be null");
			}
			result.setBusinessObjectId(boId);
			return this;
		}

		public Builder withProcess(Process process) {
			if (process != null) {
				result.setProcessId(process.getId());
				result.setEntityId(process.getEntityId());
			} else {
				result.setProcessId(null);
			}
			return this;
		}

		public Builder withSearchFilterId(String searchFilterId) {
			result.setSearchFilterId(searchFilterId);
			return this;
		}

		public Builder withTaskListId(String taskListId) {
			result.setTaskListId(taskListId);
			return this;
		}

		public WebclientUrl build() {
			return result;
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final WebclientUrl that = (WebclientUrl) o;
		return Objects.equals(businessObjectId, that.businessObjectId) &&
				Objects.equals(entityId, that.entityId) &&
				Objects.equals(processId, that.processId) &&
				Objects.equals(searchFilterId, that.searchFilterId) &&
				Objects.equals(taskListId, that.taskListId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(businessObjectId, entityId, processId, searchFilterId, taskListId);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("WebclientUrl{");
		sb.append("businessObjectId=").append(businessObjectId);
		sb.append(", entityId=").append(entityId);
		sb.append(", processId=").append(processId);
		sb.append(", searchFilterId='").append(searchFilterId).append('\'');
		sb.append(", taskListId='").append(taskListId).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
