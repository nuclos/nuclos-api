package org.nuclos.api.exception;

/**
 * BusinessException is a general exception class for <em>checked</em> 
 * exceptions in (user provided) business rules.
 * <p>
 * If a BusinessException is thrown from a (server) business rule, it 
 * is <em>guaranteed</em> that the exception message text is displayed to the 
 * nuclos client GUI user.
 * <p>
 * This class is used to display business process problems triggered by
 * business rule execution to the client user.
 * 
 * @see Exception
 * @author Matthias Reichart
 */
public class BusinessException extends Exception {

	/**
	 * for deserialization only
	 */
	protected
	BusinessException() {
		super();
	}

	/**
	 * Constructor to intiate the new NuclosBusinessException with an error message
	 * @param pMessage
	 */
	public BusinessException(String pMessage) {
		super(pMessage);
	}
	
	/**
	 * Constructor to intiate the new NuclosBusinessException with an object of type Throwable
	 * @param pThrowable
	 */
	public BusinessException(Throwable pThrowable) {
		super(pThrowable);
	}
	
	/**
	 * Constructor to intiate the new NuclosBusinessException with an object of type Throwable
	 * and an error message
	 * @param pThrowable
	 */
	public BusinessException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
	
	
	/**
	 * This method returns the message that is used in the parameter cause of type Throwable
	 * @param cause
	 * @return
	 */
	protected static String getMessage(Throwable cause) {
		if (cause == null) {
			return null;
		}
		Throwable t = cause;
		if (t.getMessage() != null && (t instanceof BusinessException)) {
			return t.getMessage();
		}
		return cause.toString();
	}
}
