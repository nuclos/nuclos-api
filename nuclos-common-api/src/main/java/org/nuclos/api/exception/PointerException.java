package org.nuclos.api.exception;


/**
 * {@link PointerException} is an exception class used in rule programming. When thrown it is displayed as 
 * a component focused speech bubble on the client.
 * 
 * @see BusinessException
 * @author Matthias Reichart
 */
public class PointerException extends BusinessException {

	String message;

	/**
	 * for deserialization only
	 */
	protected PointerException() {
		super();
	}
	
	/**
	 * Constructor to intiate the new PointerException with an error message
	 * @param pMessage
	 */
	public PointerException(String pMessage) {
		super(pMessage);
		this.message = pMessage;
	}
	
	/**
	 * This method returns the message text of the exception
	 * @return message - String
	 */
	public String getMessage() {
		return this.message;
	}
	
}
