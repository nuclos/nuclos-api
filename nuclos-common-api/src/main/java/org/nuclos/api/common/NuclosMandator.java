package org.nuclos.api.common;

import org.nuclos.api.UID;

public interface NuclosMandator {
	public UID getId();
	public String getName();
}
