//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.common;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;

public interface NuclosUserCommunicationAccount extends BusinessObject<UID> {

	/**
	 * Attribute: primaryKey
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRUID
	 *<br>Data type: org.nuclos.api.UID
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	**/
	public static final PrimaryKeyAttribute<org.nuclos.api.UID> Id = 
		new PrimaryKeyAttribute<org.nuclos.api.UID>("Id", "org.nuclos.businessentity", "T9Cd", "T9Cd0", org.nuclos.api.UID.class );
	
	/**
	 * Attribute: user
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.api.UID
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	**/
	public static final ForeignKeyAttribute<org.nuclos.api.UID> UserId = 
		new ForeignKeyAttribute<org.nuclos.api.UID>("userId", "org.nuclos.businessentity", "T9Cd", "T9Cda", org.nuclos.api.UID.class );
	
	/**
	 * Attribute: communicationPort
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRUID_T_AD_COMPORT
	 *<br>Data type: org.nuclos.api.UID
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	**/
	public static final ForeignKeyAttribute<org.nuclos.api.UID> CommunicationPortId = 
		new ForeignKeyAttribute<org.nuclos.api.UID>("CommunicationPortId", "org.nuclos.businessentity", "T9Cd", "T9Cdb", org.nuclos.api.UID.class );

	/**
	 * Attribute: account
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRACCOUNT
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	**/
	public static final StringAttribute<java.lang.String> Account = 
			new StringAttribute<java.lang.String>("Account", "org.nuclos.businessentity", "T9Cd", "T9Cdc", java.lang.String.class );
	
	/**
	 * Attribute: custom1
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRCUSTOM1
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	**/
	public static final StringAttribute<java.lang.String> Custom1 = 
			new StringAttribute<java.lang.String>("Custom1", "org.nuclos.businessentity", "T9Cd", "T9Cde", java.lang.String.class );
	
	/**
	 * Attribute: custom2
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRCUSTOM2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	**/
	public static final StringAttribute<java.lang.String> Custom2 = 
			new StringAttribute<java.lang.String>("Custom2", "org.nuclos.businessentity", "T9Cd", "T9Cdf", java.lang.String.class );
	
	/**
	 * Attribute: custom3
	 *<br>
	 *<br>Entity: nuclos_userCommunicationAccount
	 *<br>DB-Name: STRCUSTOM3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	**/
	public static final StringAttribute<java.lang.String> Custom3 = 
			new StringAttribute<java.lang.String>("Custom3", "org.nuclos.businessentity", "T9Cd", "T9Cdg", java.lang.String.class );
	
	public UID getUserId();
	public UID getCommunicationPortId();
	public String getAccount();
	public String getPassword();
	public String getCustom1();
	public String getCustom2();
	public String getCustom3();
	
	public void setAccount(String account);
	public void setPassword(String password);
	public void setCustom1(String custom1);
	public void setCustom2(String custom2);
	public void setCustom3(String custom3);
}
