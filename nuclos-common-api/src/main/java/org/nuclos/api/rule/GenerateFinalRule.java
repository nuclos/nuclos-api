//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.rule;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link GenerateRule} is the interface that should be used to make an rule
 * applicable for Afterward-Generation-Events. 
 * <p>Classes implementing this interface can be attached to workingsteps. 
 * In case of an Generation-Event the method generate is called.
 * 
 * </p>
 * @author Matthias Reichart
 */
@RuleType(name="nuclos.ruletype.generatefinalrule.name", description="nuclos.ruletype.generatefinalrule.description")
public interface GenerateFinalRule {
	
	/**
	 * {@link GenerateContext} is the context providing all generation - relevant attributes and methods
	 * like TargetObjects, SourceObjects and ParameterObjects
	 * 
	 * @param context
	 */
	public void generateFinal(GenerateContext context)throws BusinessException;
	
}
