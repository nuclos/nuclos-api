//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.rule;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link InsertFinalRule} is the interface that should be used to make an rule
 * applicable for Afterwards-Insert-Events. 
 * <p>Classes implementing this interface can be attached to entities. 
 * In case of an Insert-Event the method insert is called afterwards.
 * 
 * </p>
 * @author Matthias Reichart
 * @see InsertContext
 */
@RuleType(name="nuclos.ruletype.insertfinalrule.name", description="nuclos.ruletype.insertfinalrule.description")
public interface InsertFinalRule {
	
	/**
	 * {@link InsertContext} is the context providing all insert - relevant attributes and methods.
	 * <p>To get the {@link BusinessObject} the method getBusinessObject() can be called.
	 * 
	 * <p><b>Note</b>: Only {@link BusinessObject} / Entities that are modifiable can be used in Insert-Event.
	 * <p>If a {@link BusinessObject} cannot be used in this context check wether the entity is set modifiyable
	 * 
	 * @param context
	 */
	void insertFinal(InsertContext context) throws BusinessException;
	
}
