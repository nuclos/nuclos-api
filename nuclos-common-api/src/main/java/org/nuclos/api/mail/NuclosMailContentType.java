package org.nuclos.api.mail;

/**
 * Class that represents the type of a {@link NuclosMail}.
 * This class is normally not instantiated by the rule programmer.
 * {@link NuclosMail} provides constants of the class to use in rules.
 * @author reichama
 *
 */
public class NuclosMailContentType {

	private final String type;
	private final String charset;

	public static final NuclosMailContentType HTML  = new NuclosMailContentType("text/html", "ISO-8859-1");
	public static final NuclosMailContentType PLAIN = new NuclosMailContentType("text/plain", "UTF-8");

	/**
	 * Constructor with charset and type values used as defaults
	 * @param type mail type, e.g. text/plain or text/html
	 * @param charset charset, e.g. UTF-8 or ISO-8859-1
	 */
	public NuclosMailContentType(String type, String charset) {
		this.type = type;
		this.charset = charset;
	}

	/**
	 * Method returns the mail type, e.g. text/plain or text/html
	 * @return mail type of this NuclosMailContentType
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Method returns the charset, e.g UTF-8 or ISO-8859-1
	 * @return charset of this NuclosMailContentType
	 */
	public String getCharset() {
		return this.charset;
	}

}
