//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.mail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.api.common.NuclosFile;

/**
 * class representing an email-object
 * 
 * org.nuclos.api.provider.MailProvider
 * @author Matthias Reichart
 */
public class NuclosMail {
	
	private String nameAuthor;
	private List<String> recipients = new ArrayList<String>();
	private List<String> recipientsCC = new ArrayList<String>();
	private List<String> recipientsBCC = new ArrayList<String>();
	private String subject;
	private String message;
	private String replyTo;
	private Map<String, String> headers = new HashMap<String, String>();
	private final Collection<NuclosFile> lstAttachment = new ArrayList<NuclosFile>(1);
	private final Collection<NuclosFile> lstInlineAttachment = new ArrayList<NuclosFile>(1);
	private String mail_type;
	private String mail_charset;

	private Date receivedDate;
	private Date sentDate;

	public static final NuclosMailContentType HTML  = new NuclosMailContentType("text/html", "ISO-8859-1");
	public static final NuclosMailContentType PLAIN = new NuclosMailContentType("text/plain", "UTF-8");
	
	public NuclosMail() {
		this.mail_charset = NuclosMailContentType.PLAIN.getCharset();
		this.mail_type = NuclosMailContentType.PLAIN.getType();
	}
	
	public NuclosMail(String sRecipient, String sSubject) {
		super();
		addRecipient(sRecipient);
		this.setSubject(sSubject);
	}
	
	/**
	 * sets the name of the sender
	 * @param pNameAuthor
	 */
	public void setFrom(String pNameAuthor) {
		this.nameAuthor = pNameAuthor;
	}
	
	/**
	 * returns the name of the sender
	 * @return
	 */
	public String getFrom() {
		return this.nameAuthor;
	}
	
	
	/**
	 * Use this method to set the mail type of the current {@link NuclosMail}.<br>
	 * For {@link NuclosMailContentType} use NuclosMail constant values.
	 * @param encoding
	 */
	public void setContentType(NuclosMailContentType encoding) {
		this.mail_type = encoding.getType();
		this.mail_charset = encoding.getCharset();
	}
	
	/**
	 * adds a new recipient; 
	 * the given string value will be checked if there is more then one recipient mentioned (semicolon separated)
	 * @param emailRecipient
	 */
	public void addRecipient(String emailRecipient) {
		if (emailRecipient != null) {
			String[] split = emailRecipient.split(";");
			for(String add : split) {
				this.recipients.add(add.trim());				
			}
		}
	}

	
	/**
	 * returns a list of all recipients
	 * @return
	 */
	public List<String> getRecipients() {
		return this.recipients;
	}
	
	/**
	 * adds a new CC recipient; 
	 * the given string value will be checked if there is more then one recipient mentioned (semicolon separated)
	 * @param emailRecipientCC
	 */
	public void addRecipientCC(String emailRecipientCC) {
		if (emailRecipientCC != null) {
			String[] split = emailRecipientCC.split(";");
			for(String add : split) {
				this.recipientsCC.add(add.trim());				
			}
		}
	}

	/**
	 * returns a list of all recipientsCC
	 * @return
	 */
	public List<String> getRecipientsCC() {
		return this.recipientsCC;
	}

	/**
	 * adds a new BBC recipient; 
	 * the given string value will be checked if there is more then one recipient mentioned (semicolon separated)
	 * @param emailRecipientBCC
	 */
	public void addRecipientBCC(String emailRecipientBCC) {
		if (emailRecipientBCC != null) {
			String[] split = emailRecipientBCC.split(";");
			for(String add : split) {
				this.recipientsBCC.add(add.trim());				
			}
		}
	}

	/**
	 * returns a list of all recipientsBCC
	 * @return
	 */
	public List<String> getRecipientsBCC() {
		return this.recipientsBCC;
	}
	
	
	/**
	 * sets the subject of the email
	 * @param pEmailsubject
	 */
	public void setSubject(String pEmailsubject) {
		this.subject = pEmailsubject;
	}
	
	/**
	 * sets the message text of the email
	 * @param pEmailMessage
	 */
	public void setMessage(String pEmailMessage) {
		this.message = pEmailMessage;
	}
	
	/**
	 * sets the reply address of the email
	 * @param pEmailReplyTo
	 */
	public void setReplyTo(String pEmailReplyTo) {
		this.replyTo = pEmailReplyTo;
	}

	
	/**
	 * attaches a {@link NuclosFile} to the email
	 * @param pFileToAttach
	 */
	public void addAttachment(NuclosFile pFileToAttach) {
		this.lstAttachment.add(pFileToAttach);
	}


	/**
	 * Attaches a {@link NuclosFile} representing an inline attachment (e.g. an embedded image) to the email.
	 *
	 * @param pFileToAttach
	 */
	public void addInlineAttachment(NuclosFile pFileToAttach) {
		this.lstInlineAttachment.add(pFileToAttach);
	}

	/**
	 * adds an entry to the header object of the email
	 * @param pProperty - key
	 * @param pValue - value
	 */
	public void setHeader(String pProperty, String pValue) {
		this.headers.put(pProperty, pValue);
	}

	
	/**
	 * returns the subject of the email
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * returns the message text of the email
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * returns the reply address of the email
	 * @return
	 */
	public String getReplyTo() {
		return replyTo;
	}
	
	/**
	 * returns a collection of all attached {@link NuclosFile}s
	 * @return
	 */
	public Collection<NuclosFile> getAttachments() {
		return this.lstAttachment;
	}

	/**
	 * Returns a collection of all attached {@link NuclosFile}s representing inline attachments (e.g. embedded images).
	 *
	 * @return
	 */
	public Collection<NuclosFile> getInlineAttachments() {
		return this.lstInlineAttachment;
	}

	
	/**
	 * returns a map with all header information
	 * @return
	 */
	public Map<String, String> getHeaders() {
		return headers;
	}
	

	/**
	 * returns the mail type, e.g. text/html or text/plain
	 * @return
	 */
	public String getMailType() {
		return this.mail_type;
	}
	
	/**
	 * returns the mail charset, e.g. UTF-8
	 * @return
	 */
	public String getMailCharset() {
		return this.mail_charset;
	}


	/**
	 * Gets the date this mail was received by the server.
	 *
	 * @return
	 */
	public Date getReceivedDate() {
		return receivedDate == null ? null : new Date(receivedDate.getTime());
	}

	/**
	 * Sets the date this mail was received by the server.
	 */
	public void setReceivedDate(final Date receivedDate) {
		this.receivedDate = receivedDate == null ? null : new Date(receivedDate.getTime());
	}

	/**
	 * Gets the date this mail was sent.
	 *
	 * @return
	 */
	public Date getSentDate() {
		return sentDate == null ? null : new Date(sentDate.getTime());
	}

	/**
	 * Sets the date this mail was sent.
	 */
	public void setSentDate(final Date sentDate) {
		this.sentDate = sentDate == null ? null : new Date(sentDate.getTime());
	}
}
