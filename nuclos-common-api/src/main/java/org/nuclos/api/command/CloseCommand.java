package org.nuclos.api.command;

/**
 * Commands the client to close the current window/tab.
 *
 * @author Andreas Lämmlein
 */
public class CloseCommand implements Command {
}
