package org.nuclos.api.parameter;

/**
 * This class represents a nuclet parameter that has been created in Nuclos; It is usually not used
 * by rule programmers directly, but it is part of a NucletParameters-Class (e.g. PaymentNucletParameter) that is created during 
 * runtime containing all parameters of the corresponding nuclet as NucletParameter-instances.
 * 
 * @author reichama
 *
 */
public class NucletParameter {

	private String id;
	
	/**
	 * Constructor to set a new instance of the NucletParameter class
	 * using the id of a parameter that has been created in Nuclos previously
	 * 
	 * Internal use only
	 * 
	 * @param id - UID of the parameter as String
	 */
	public NucletParameter(String id) {
		this.id = id;
	}
	
	/**
	 * This method returns the id of the current nuclet parameter
	 * @return
	 */
	public String getId() {
		return this.id;
	}

}
