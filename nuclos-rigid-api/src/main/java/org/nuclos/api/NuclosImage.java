//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.package org.nuclos.api.common;
package org.nuclos.api;

/**
 * Class representing an image used in Nuclos
 * @author reichama
 *
 */
public interface NuclosImage {

	/**
	 * Returns the filename of the image
	 * @return
	 */
	public String getName() ;

	/**
	 * Returns the content of the image
	 * @return
	 */
	public byte[] getContent() ;
	
}
